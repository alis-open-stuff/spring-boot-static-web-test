package de.cyface.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController(value="/test")
public class TestController {
    
    @RequestMapping(method=RequestMethod.GET)
    public String get() {
        return "controller";
    }

}
