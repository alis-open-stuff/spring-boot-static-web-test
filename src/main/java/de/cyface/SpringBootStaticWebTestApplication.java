package de.cyface;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootStaticWebTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootStaticWebTestApplication.class, args);
	}
}
